#!/usr/bin/env bash

rm -rf $HOME/.vim
cp -r ./config/.vim $HOME/.vim
cp ./config/.vimrc $HOME

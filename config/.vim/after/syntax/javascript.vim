" Javascript Vim syntax file
" @author Giovanni Macciocu
" @date Sun Apr  2 00:44:04 UTC 2017

" ******************************************************** "
" ********************* MyOperators ********************** "
" ******************************************************** "

syntax match MySemicolon "\v\;"
syntax match MyColon "\v\:"
syntax match MyDot "\v\."
syntax match MyComma "\v\,"
syntax match MyBrackets "\v\("
syntax match MyBrackets "\v\)"
syntax match MySqBrackets "\v\["
syntax match MySqBrackets "\v\]"
syntax match MyCurlyBraces "\v\{"
syntax match MyCurlyBraces "\v\}"
syntax match MyOperators "\v\+"
syntax match MyOperators "\v\-"
syntax match MyOperators "\v\="
syntax match MyOperators "\v\<"
syntax match MyOperators "\v\>"
syntax match MyOperators "\v\*"
syntax match MyOperators "\v\!"
syntax match MyOperators "\v\?"
syntax match MyOperators "\v\&"
syntax match MyOperators "\v\|"

" ******************************************************** "
" ********************** es6 syntax ********************** "
" ******************************************************** "
syn keyword es6 of constructor Math

" ******************************************************** "
" ************************* link ************************* "
" ******************************************************** "

hi link es6 Function

" PostgreSQL Vim syntax file
" @author Giovanni Macciocu
" @date Thu 05 May 2022 11:19:06 PM CEST

" ******************************************************** "
" ********************* MyOperators ********************** "
" ******************************************************** "

syntax match MySemicolon "\v\;"
syntax match MyColon "\v\:"
syntax match MyDot "\v\."
syntax match MyComma "\v\,"
syntax match MyBrackets "\v\("
syntax match MyBrackets "\v\)"
syntax match MySqBrackets "\v\["
syntax match MySqBrackets "\v\]"
syntax match MyCurlyBraces "\v\{"
syntax match MyCurlyBraces "\v\}"
syntax match MyOperators "\v\+"
syntax match MyOperators "\v\="
syntax match MyOperators "\v\<"
syntax match MyOperators "\v\>"
syntax match MyOperators "\v\*"
syntax match MyOperators "\v\!"
syntax match MyOperators "\v\?"
syntax match MyOperators "\v\&"
syntax match MyOperators "\v\|"

syntax keyword postgreSQLKeyword double serial timestamptz
syntax keyword postgreSQLIdentifier constraint primary key references

highlight default link postgreSQLKeyword Type
highlight default link postgreSQLIdentifier Identifier

" GoLang ++ Vim syntax file
" @author Giovanni Macciocu
" @date Sun 25 Jul 2021 02:55:36 PM CEST

" ******************************************************** "
" ********************* MyOperators ********************** "
" ******************************************************** "

syntax match MySemicolon "\v\;"
syntax match MyColon "\v\:"
syntax match MyDot "\v\."
syntax match MyComma "\v\,"
syntax match MyBrackets "\v\("
syntax match MyBrackets "\v\)"
syntax match MySqBrackets "\v\["
syntax match MySqBrackets "\v\]"
syntax match MyCurlyBraces "\v\{"
syntax match MyCurlyBraces "\v\}"
syntax match MyOperators "\v\+"
syntax match MyOperators "\v\-"
syntax match MyOperators "\v\="
syntax match MyOperators "\v\<"
syntax match MyOperators "\v\>"
syntax match MyOperators "\v\*"
syntax match MyOperators "\v\!"
syntax match MyOperators "\v\?"
syntax match MyOperators "\v\&"
syntax match MyOperators "\v\|"

" Dockerfile ++ Vim syntax file
" @author Giovanni Macciocu
" @date Tue 14 Sep 2021 04:33:59 PM CEST

" ******************************************************** "
" ********************* MyOperators ********************** "
" ******************************************************** "

syntax match MySemicolon "\v\;"
syntax match MyColon "\v\:"
syntax match MyDot "\v\."
syntax match MyComma "\v\,"
syntax match MyBrackets "\v\("
syntax match MyBrackets "\v\)"
syntax match MySqBrackets "\v\["
syntax match MySqBrackets "\v\]"
syntax match MyCurlyBraces "\v\{"
syntax match MyCurlyBraces "\v\}"
syntax match MyOperators "\v\+"
syntax match MyOperators "\v\-"
syntax match MyOperators "\v\="
syntax match MyOperators "\v\<"
syntax match MyOperators "\v\>"
syntax match MyOperators "\v\*"
syntax match MyOperators "\v\!"
syntax match MyOperators "\v\?"
syntax match MyOperators "\v\&"
syntax match MyOperators "\v\|"

" Python Vim syntax file

" ******************************************************** "
" ********************* MyOperators ********************** "
" ******************************************************** "

syntax match MySemicolon "\v\;"
syntax match MyColon "\v\:"
syntax match MyDot "\v\."
syntax match MyComma "\v\,"
syntax match MyBrackets "\v\("
syntax match MyBrackets "\v\)"
syntax match MySqBrackets "\v\["
syntax match MySqBrackets "\v\]"
syntax match MyCurlyBraces "\v\{"
syntax match MyCurlyBraces "\v\}"
syntax match MyOperators "\v\+"
syntax match MyOperators "\v\-"
syntax match MyOperators "\v\="
syntax match MyOperators "\v\<"
syntax match MyOperators "\v\>"
syntax match MyOperators "\v\*"
syntax match MyOperators "\v\!"
syntax match MyOperators "\v\?"
syntax match MyOperators "\v\&"
syntax match MyOperators "\v\|"

" ******************************************************** "
" ************************ syntax ************************ "
" ******************************************************** "

syn keyword pythonStatement     break continue del
syn keyword pythonStatement     except exec finally
syn keyword pythonStatement     pass print raise
syn keyword pythonStatement     return try with
syn keyword pythonStatement     global assert
syn keyword pythonStatement     lambda yield

syn keyword pythonRepeat	for while
syn keyword pythonConditional	if elif else
syn keyword pythonOperator	and in is not or self
" AS will be a keyword in Python 3
syn keyword pythonPreCondit	import from as
syn keyword pythonTodo		TODO FIXME XXX contained

" Decorators (new in Python 2.4)
syn match   pythonDecorator	"@" display nextgroup=pythonFunction skipwhite

" strings
syn region pythonString		start=+[uU]\='+ end=+'+ skip=+\\\\\|\\'+ contains=pythonEscape,@Spell
syn region pythonString		start=+[uU]\="+ end=+"+ skip=+\\\\\|\\"+ contains=pythonEscape,@Spell
syn region pythonString		start=+[uU]\="""+ end=+"""+ contains=pythonEscape,@Spell
syn region pythonString		start=+[uU]\='''+ end=+'''+ contains=pythonEscape,@Spell
syn region pythonRawString	start=+[uU]\=[rR]'+ end=+'+ skip=+\\\\\|\\'+ contains=@Spell
syn region pythonRawString	start=+[uU]\=[rR]"+ end=+"+ skip=+\\\\\|\\"+ contains=@Spell
syn region pythonRawString	start=+[uU]\=[rR]"""+ end=+"""+ contains=@Spell
syn region pythonRawString	start=+[uU]\=[rR]'''+ end=+'''+ contains=@Spell
syn match  pythonEscape		+\\[abfnrtv'"\\]+ contained
syn match  pythonEscape		"\\\o\{1,3}" contained
syn match  pythonEscape		"\\x\x\{2}" contained
syn match  pythonEscape		"\(\\u\x\{4}\|\\U\x\{8}\)" contained
syn match  pythonEscape		"\\$"

" comments
syn region pythonComment
      \ start=+\%(:\n\s*\)\@<=\z('''\|"""\)+ end=+\z1+ keepend
            \ contains=pythonEscape,pythonTodo,@Spell

" numbers (including longs and complex)
syn match   pythonNumber	"\<0x\x\+[Ll]\=\>"
syn match   pythonNumber	"\<\d\+[LljJ]\=\>"
syn match   pythonNumber	"\.\d\+\([eE][+-]\=\d\+\)\=[jJ]\=\>"
syn match   pythonNumber	"\<\d\+\.\([eE][+-]\=\d\+\)\=[jJ]\=\>"
syn match   pythonNumber	"\<\d\+\.\d\+\([eE][+-]\=\d\+\)\=[jJ]\=\>"

" builtin functions, types and objects, not really part of the syntax
syn keyword pythonBuiltin	True False bool enumerate set frozenset help
syn keyword pythonBuiltin	reversed sorted sum
syn keyword pythonBuiltin	Ellipsis None NotImplemented __import__ abs
syn keyword pythonBuiltin	apply buffer callable chr classmethod cmp
syn keyword pythonBuiltin	coerce compile complex delattr dict dir divmod
syn keyword pythonBuiltin	eval execfile file filter float getattr globals
syn keyword pythonBuiltin	hasattr hash hex id input int intern isinstance
syn keyword pythonBuiltin	issubclass iter len list locals long map max
syn keyword pythonBuiltin	min object oct open ord pow property range
syn keyword pythonBuiltin	raw_input reduce reload repr round setattr
syn keyword pythonBuiltin	slice staticmethod str super tuple type unichr
syn keyword pythonBuiltin	unicode vars xrange zip

" builtin exceptions and warnings
syn keyword pythonException	ArithmeticError AssertionError AttributeError
syn keyword pythonException	DeprecationWarning EOFError EnvironmentError
syn keyword pythonException	Exception FloatingPointError IOError
syn keyword pythonException	ImportError IndentationError IndexError
syn keyword pythonException	KeyError KeyboardInterrupt LookupError
syn keyword pythonException	MemoryError NameError NotImplementedError
syn keyword pythonException	OSError OverflowError OverflowWarning
syn keyword pythonException	ReferenceError RuntimeError RuntimeWarning
syn keyword pythonException	StandardError StopIteration SyntaxError
syn keyword pythonException	SyntaxWarning SystemError SystemExit TabError
syn keyword pythonException	TypeError UnboundLocalError UnicodeError
syn keyword pythonException	UnicodeEncodeError UnicodeDecodeError
syn keyword pythonException	UnicodeTranslateError
syn keyword pythonException	UserWarning ValueError Warning WindowsError
syn keyword pythonException	ZeroDivisionError

" trailing whitespace
syn match   pythonSpaceError   display excludenl "\S\s\+$"ms=s+1
" mixed tabs and spaces
syn match   pythonSpaceError   display " \+\t"
syn match   pythonSpaceError   display "\t\+ "

" This is fast but code inside triple quoted strings screws it up. It
" is impossible to fix because the only way to know if you are inside a
" triple quoted string is to start from the beginning of the file. If
" you have a fast machine you can try uncommenting the "sync minlines"
" and commenting out the rest.
"syn sync match pythonSync grouphere NONE "):$"
"syn sync maxlines=200
syn sync minlines=2000
syn sync linebreaks=1

" ******************************************************** "
" ************************* link ************************* "
" ******************************************************** "

hi link pythonStatement          Statement
hi link pythonDefStatement	Statement
hi link pythonFunction		Function
hi link pythonConditional	Conditional
hi link pythonRepeat		Repeat
hi link pythonString		String
hi link pythonRawString	        String
hi link pythonEscape		Special
hi link pythonOperator		Operator
hi link pythonPreCondit	        PreCondit
hi link pythonComment		Comment
hi link pythonTodo		Todo
hi link pythonDecorator	        Define
hi link pythonNumber	        Number
hi link pythonBuiltin	        Function
hi link pythonException	        Exception
hi link pythonSpaceError	Error

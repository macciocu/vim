" Vim color theme file
" author: Giovanni Macciocu
" date: Sun May 19 16:11:05 2019

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=dark
let g:colors_name="black"

" Syntax
hi Normal         guibg=#050505       guifg=#317042
hi Comment        guibg=NONE          guifg=#424242       cterm=italic
hi String         guibg=NONE          guifg=#556c2a       cterm=NONE
hi Title          ctermbg=NONE        guifg=#4c668c       cterm=NONE " strings, markdown sections, html headers
hi Constant       guibg=NONE          guifg=#6f9ea3       cterm=NONE " numbers
hi Identifier     guibg=NONE          guifg=#6b6091       cterm=NONE
hi Statement      guibg=NONE          guifg=#668c4c       cterm=bold
hi PreProc        guibg=NONE          guifg=#6b6091       cterm=bold
hi Type           guibg=NONE          guifg=#6b6091       cterm=bold
hi Special        guibg=NONE          guifg=#6b6091       cterm=bold
hi Underlined     guibg=NONE          guifg=#454545       cterm=bold
hi Ignore         guibg=NONE          guifg=#dd5d0e       cterm=bold
hi Error          guibg=NONE          guifg=#cc241d       cterm=bold
hi SpellBad       guibg=#a83232       guifg=#050505       cterm=NONE " error highlight coloring
hi SpellCap       guibg=#a87d32       guifg=#050505       cterm=NONE " warning highlight coloring
hi! link String Title
hi! link Todo Error

" UI
hi LineNr         guibg=#050505       guifg=#252525       cterm=NONE
hi NonText        guibg=NONE          guifg=NONE          cterm=NONE
hi SpecialKey     guibg=NONE          guifg=#3f4151       cterm=NONE
hi Search         guibg=#4f903e       guifg=#050505       cterm=NONE
hi Visual         guibg=#202040       guifg=NONE          cterm=NONE
hi PMenu          guibg=#004c8e       guifg=#ffffff       cterm=NONE
hi PMenuSel       guibg=#ffffff       guifg=#004c8e       cterm=NONE
hi Directory      guibg=NONE          guifg=#687bb4       cterm=NONE
hi Folded         guibg=#150000       guifg=#7d7b77       cterm=NONE
hi FoldColumn     guibg=#150000       guifg=NONE          cterm=NONE
hi CursorLine     guibg=#070707       guifg=NONE          cterm=NONE
hi CursorLineNr   guibg=#050505       guifg=#b5ac72       cterm=NONE
hi StatusLine     guibg=#101010       guifg=#007e9d       cterm=NONE
hi StatusLineNC   guibg=#101010       guifg=#707070       cterm=NONE
hi NonText        guibg=#050505       guifg=#404040       cterm=NONE  " e.g. '~' (end of buffer), terminal prompt
hi Wildmenu       guibg=#202020       guifg=#bf373a       cterm=bold
hi MatchParen     guibg=#4f903e       guifg=#dddddd       cterm=bold
hi! link VertSplit LineNr
hi! link SignColumn LineNr
"hi! link StatusLineTerm StatusLine
"hi! link StatusLineTermNC StatusLineNC

" UI Tabs
hi TabLineFill    guibg=#101010       guifg=#ffffff       cterm=NONE
hi! link TabLine StatusLineNC
hi! link TabLineSel StatusLine

" Diff
hi DiffAdd        guibg=#3a4f6c       guifg=#101010
hi DiffDelete     guibg=#602020       guifg=#101010
hi DiffChange     guibg=#1a5c2e       guifg=#101010

syntax match MySemicolon "\v\;"
syntax match MyColon "\v\:"
syntax match MyDot "\v\."
syntax match MyComma "\v\,"
syntax match MyBrackets "\v\("
syntax match MyBrackets "\v\)"
syntax match MySqBrackets "\v\["
syntax match MySqBrackets "\v\]"
syntax match MyCurlyBraces "\v\{"
syntax match MyCurlyBraces "\v\}"
syntax match MyOperators "\v\+"
syntax match MyOperators "\v\="
syntax match MyOperators "\v\<"
syntax match MyOperators "\v\>"
syntax match MyOperators "\v\*"
syntax match MyOperators "\v\!"
syntax match MyOperators "\v\?"

hi MySemicolon    guifg=#b5ac72
hi MyColon        guifg=#b5ac72
hi MyDot          guifg=#b5ac72
hi MyComma        guifg=#b5ac72
hi MyBrackets     guifg=#668c4c
hi MySqBrackets   guifg=#668c4c
hi MyCurlyBraces  guifg=#668c4c
hi MyOperators    guifg=#a0a0a0

" Vim color theme file
" author: Giovanni Macciocu
" date: Sun May 19 16:11:05 2019

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=dark
let g:colors_name="brown_green"

" Syntax
hi Normal         guibg=#161616       guifg=#6d9c67
hi Comment        guibg=#161616       guifg=#767066       cterm=italic
hi String         guibg=NONE          guifg=#8688b1       cterm=NONE
hi Title          guibg=NONE          guifg=#c86363       cterm=NONE " markdown sections, html headers
hi Constant       guibg=NONE          guifg=#ebdbb2       cterm=bold " numbers
hi Identifier     guibg=NONE          guifg=#d7bf8f       cterm=NONE
hi Statement      guibg=NONE          guifg=#98971a       cterm=bold
hi PreProc        guibg=NONE          guifg=#d79921       cterm=bold
hi Type           guibg=NONE          guifg=#458588       cterm=bold
hi Special        guibg=NONE          guifg=#8ec07c       cterm=bold
hi Underlined     guibg=NONE          guifg=#454545       cterm=bold
hi Ignore         guibg=NONE          guifg=#dd5d0e       cterm=bold
hi Error          guibg=NONE          guifg=#cc241d       cterm=bold
hi SpellBad       guibg=#a87d32       guifg=#000000       cterm=NONE " error highlight coloring
hi SpellCap       guibg=#a83232       guifg=#000000       cterm=NONE " warning highlight coloring
hi! link Todo Error

" UI
hi LineNr         guibg=#161616       guifg=#454545       cterm=NONE
hi NonText        guibg=NONE          guifg=NONE          cterm=NONE
hi SpecialKey     guibg=NONE          guifg=#3f4151       cterm=NONE
hi Search         guibg=#4E7190       guifg=#dddddd       cterm=NONE
hi Visual         guibg=#404040       guifg=NONE          cterm=NONE
hi PMenu          guibg=#004c8e       guifg=#ffffff       cterm=NONE
hi PMenuSel       guibg=#ffffff       guifg=#004c8e       cterm=NONE
hi Directory      guibg=NONE          guifg=#687bb4       cterm=NONE
hi Folded         guibg=#161616       guifg=#7d7b77       cterm=NONE
hi FoldColumn     guibg=#161616       guifg=NONE          cterm=NONE
hi CursorLine     guibg=#262626       guifg=NONE          cterm=NONE
hi CursorLineNr   guibg=#202020       guifg=#c0c0c0       cterm=NONE
hi VertSplit      guibg=#161616       guifg=#454545       cterm=NONE
hi StatusLine     guibg=#161616       guifg=#996739       cterm=bold
hi StatusLineNC   guibg=#161616       guifg=#824545       cterm=italic
hi Wildmenu       guibg=#161616       guifg=#98971a       cterm=bold
hi! link NonText LineNr
hi! link SignColumn LineNr
hi! link StatusLineTerm StatusLine
hi! link StatusLineTermNC StatusLineNC
hi! link MatchParen Search

" UI Tabs
hi TabLineFill    guibg=#161616       guifg=#ffffff       cterm=NONE
hi! link TabLine StatusLineNC
hi! link TabLineSel StatusLine

" Diff
hi DiffAdd        guibg=#006000       guifg=#ffffff
hi DiffDelete     guibg=#600000       guifg=#ffffff
hi DiffChange     guibg=#000060       guifg=#ffffff

hi MySemicolon    guifg=#6da9b8
hi MyColon        guifg=#e3655b
hi MyDot          guifg=#f3c677
hi MyComma        guifg=#60993e
hi MyBrackets     guifg=#8fa7d7
hi MySqBrackets   guifg=#13a4b9
hi MyCurlyBraces  guifg=#8f8fd7
hi MyOperators    guifg=#d9d3cb

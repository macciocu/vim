" Vim color theme file
" author: Giovanni Macciocu
" date: Sun May 19 16:11:05 2019
" Based of wezterm FirefoxDev color scheme

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=dark
let g:colors_name="ffdev"

" Syntax
hi Normal         guibg=#0e1011       guifg=#7c8fa4
hi Comment        guibg=#0e1011       guifg=#305040
hi Title          guibg=NONE          guifg=#5c9450       cterm=NONE " strings, markdown sections, html headers
hi Special        guibg=NONE          guifg=#909090       cterm=bold " special characters in strings
hi Constant       guibg=NONE          guifg=#75a886       cterm=NONE " numbers
hi Identifier     guibg=NONE          guifg=#7b70a1       cterm=bold
hi Statement      guibg=NONE          guifg=#8fa7d7       cterm=bold
hi PreProc        guibg=NONE          guifg=#75a886       cterm=bold
hi Type           guibg=NONE          guifg=#75a8b6       cterm=bold
hi Underlined     guibg=NONE          guifg=#454545       cterm=bold
hi Ignore         guibg=NONE          guifg=#dd5d0e       cterm=bold
hi Error          guibg=NONE          guifg=#cc241d       cterm=bold
hi SpellBad       guibg=#a83232       guifg=#000000       cterm=NONE " error highlight coloring
hi SpellCap       guibg=#a87d32       guifg=#000000       cterm=NONE " warning highlight coloring
hi! link String Title
hi! link Todo Error

" UI
hi LineNr         guibg=#0e1011       guifg=#2e3031       cterm=NONE
hi NonText        guibg=NONE          guifg=NONE          cterm=NONE
hi SpecialKey     guibg=NONE          guifg=#3f4151       cterm=NONE
hi Search         guibg=#462636       guifg=#dddddd       cterm=NONE
hi Visual         guibg=#462636       guifg=NONE          cterm=NONE
hi PMenu          guibg=#004c8e       guifg=#ffffff       cterm=NONE
hi PMenuSel       guibg=#ffffff       guifg=#004c8e       cterm=NONE
hi Directory      guibg=NONE          guifg=#687bb4       cterm=NONE
hi Folded         guibg=#0e1011       guifg=#7d7b77       cterm=NONE
hi FoldColumn     guibg=#0e1011       guifg=NONE          cterm=NONE
hi CursorLine     guibg=#161819       guifg=NONE          cterm=NONE
hi CursorLineNr   guibg=#161819       guifg=#264636       cterm=NONE
hi VertSplit      guibg=#0e1011       guifg=#454545       cterm=NONE
hi StatusLine     guibg=#264636       guifg=#000000       cterm=NONE
hi StatusLineNC   guibg=#303030       guifg=#000000       cterm=NONE
hi Wildmenu       guibg=#0e1011       guifg=#98971a       cterm=bold
hi! link NonText LineNr
hi! link SignColumn LineNr
hi! link StatusLineTerm StatusLine
hi! link StatusLineTermNC StatusLineNC
hi! link MatchParen Search

" UI Tabs
hi TabLineFill    guibg=#0e1011       guifg=#ffffff       cterm=NONE
hi! link TabLine StatusLineNC
hi! link TabLineSel StatusLine

" Diff
hi DiffAdd        guibg=#264636       guifg=#000000       cterm=NONE
hi DiffDelete     guibg=#462636       guifg=#000000       cterm=NONE
hi DiffChange     guibg=#105090       guifg=#000000       cterm=NONE

hi MySemicolon    guifg=#6da9b8
hi MyColon        guifg=#e3655b
hi MyDot          guifg=#f3c677
hi MyComma        guifg=#60993e
hi MyBrackets     guifg=#b63776
hi MySqBrackets   guifg=#13a4b9
hi MyCurlyBraces  guifg=#b63776
hi MyOperators    guifg=#d9d3cb

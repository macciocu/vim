" Vim color theme file
" author: Giovanni Macciocu
" date: Sun May 19 16:11:05 2019

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=light
let g:colors_name="midday"

" Syntax
hi Normal         guibg=#31363B       guifg=#bbbbbb       cterm=NONE
hi Comment        guibg=NONE          guifg=#969183       cterm=NONE
hi Title          guibg=NONE          guifg=#93b886       cterm=NONE  " markdown sections, html headers, strings
hi Constant       guibg=NONE          guifg=#b6c27c       cterm=NONE  " numbers
hi Identifier     guibg=NONE          guifg=#bcbf97       cterm=NONE
hi Statement      guibg=NONE          guifg=#c0c060       cterm=NONE
hi PreProc        guibg=NONE          guifg=#c88a63       cterm=NONE
hi Type           guibg=NONE          guifg=#aaa3d1       cterm=NONE
hi Special        guibg=NONE          guifg=#c8af6f       cterm=NONE
hi Underlined     guibg=NONE          guifg=#959595       cterm=NONE
hi Ignore         guibg=NONE          guifg=#d24300       cterm=NONE
hi Error          guibg=NONE          guifg=#d10e44       cterm=NONE
hi Wildmenu       guibg=#161616       guifg=#98971a       cterm=bold
hi SpellBad       guibg=#a83232       guifg=#000000       cterm=NONE " error highlight coloring
hi SpellCap       guibg=#a87d32       guifg=#000000       cterm=NONE " warning highlight coloring
hi! link String Title
hi! link Todo Error

" UI
hi LineNr         guibg=#31363B       guifg=#6f6f6f       cterm=NONE
hi NonText        guibg=#31363B       guifg=#454a4f       cterm=NONE " e.g. '~' (end of buffer), terminal prompt
hi SpecialKey     guibg=NONE          guifg=#3f4151       cterm=NONE
hi Search         guibg=#909020       guifg=#101010       cterm=NONE
hi Visual         guibg=#151515       guifg=NONE          cterm=NONE
hi PMenu          guibg=#004c8e       guifg=#ffffff       cterm=NONE
hi PMenuSel       guibg=#ffffff       guifg=#004c8e       cterm=NONE
hi Directory      guibg=NONE          guifg=#97aec2       cterm=bold
hi Folded         guibg=#4a5057       guifg=#7d7b77       cterm=NONE
hi FoldColumn     guibg=#4a5057       guifg=NONE          cterm=NONE
hi CursorLine     guibg=#363b40       guifg=NONE          cterm=NONE
hi CursorLineNr   guibg=#363b40       guifg=#aaa54e       cterm=NONE
hi StatusLine     guibg=#61666B       guifg=#a0a0a0       cterm=bold
hi StatusLineNC   guibg=#41464B       guifg=#a0a0a0       cterm=italic
hi! link VertSplit LineNr
hi! link SignColumn LineNr
hi! link StatusLineTerm StatusLine
hi! link StatusLineTermNC StatusLineNC
hi! link MatchParen Search

" UI Tabs
hi TabLineFill    guibg=#252525       guifg=#ffffff       cterm=NONE
hi! link TabLine StatusLineNC
hi! link TabLineSel StatusLine

" Diff
hi DiffAdd        guibg=#536a70       guifg=#c0c0c0
hi DiffDelete     guibg=#6b1902       guifg=#c0c0c0
hi DiffChange     guibg=#487251       guifg=#c0c0c0

hi MySemicolon    guifg=#b5ac72
hi MyColon        guifg=#b5ac72
hi MyDot          guifg=#b5ac72
hi MyComma        guifg=#b5ac72
hi MyBrackets     guifg=#b5ac72
hi MySqBrackets   guifg=#b5ac72
hi MyCurlyBraces  guifg=#b5ac72
hi MyOperators    guifg=#b5ac72

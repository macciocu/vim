" Vim color theme file
" author: Giovanni Macciocu
" date: Sun May 19 16:11:05 2019

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=light
let g:colors_name="dawn"

" Syntax
hi Normal         guibg=#e0e0ce       guifg=#202020       cterm=NONE
hi Comment        guibg=NONE          guifg=#886230       cterm=NONE
hi Title          guibg=NONE          guifg=#005d5d       cterm=NONE  " markdown sections, html headers
hi Constant       guibg=NONE          guifg=#024280       cterm=NONE  " numbers
hi Identifier     guibg=NONE          guifg=#2a3153       cterm=NONE
hi Statement      guibg=NONE          guifg=#024280       cterm=NONE
hi PreProc        guibg=NONE          guifg=#5d0000       cterm=NONE
hi Type           guibg=NONE          guifg=#075b00       cterm=NONE
hi Special        guibg=NONE          guifg=#024280       cterm=NONE
hi Underlined     guibg=NONE          guifg=#454545       cterm=NONE
hi Ignore         guibg=NONE          guifg=#d24300       cterm=NONE
hi Error          guibg=NONE          guifg=#d10e44       cterm=NONE
hi SpellBad       guibg=#a83232       guifg=#000000       cterm=NONE " error highlight coloring
hi SpellCap       guibg=#a87d32       guifg=#000000       cterm=NONE " warning highlight coloring
hi! link String Title
hi! link Todo Error

" UI
hi LineNr         guibg=#e0e0ce       guifg=#797773       cterm=NONE
hi NonText        guibg=NONE          guifg=NONE          cterm=NONE
hi SpecialKey     guibg=NONE          guifg=#3f4151       cterm=NONE
hi Search         guibg=#aaddff       guifg=#202020       cterm=NONE
hi Visual         guibg=#ffffaa       guifg=NONE          cterm=NONE
hi PMenu          guibg=#004c8e       guifg=#ffffff       cterm=NONE
hi PMenuSel       guibg=#ffffff       guifg=#004c8e       cterm=NONE
hi Directory      guibg=NONE          guifg=#4a5700       cterm=NONE
hi Folded         guibg=#ffffff       guifg=#7d7b77       cterm=NONE
hi FoldColumn     guibg=#ffffff       guifg=NONE          cterm=NONE
hi CursorLine     guibg=#edede1       guifg=NONE          cterm=NONE
hi CursorLineNr   guibg=#edede1       guifg=#8e0000       cterm=NONE
hi StatusLine     guibg=#edede1       guifg=#252525       cterm=NONE
hi StatusLineNC   guibg=#d0d0be       guifg=#252525       cterm=NONE
hi NonText        guibg=#e0e0ce       guifg=#797773       cterm=NONE  " e.g. '~' (end of buffer), terminal prompt
hi Wildmenu       guibg=#ffffff       guifg=#024280       cterm=bold
hi! link VertSplit LineNr
hi! link SignColumn LineNr
hi! link StatusLineTerm StatusLine
hi! link StatusLineTermNC StatusLineNC
hi! link MatchParen Search

" UI Tabs
hi TabLineFill    guibg=#252525       guifg=#ffffff       cterm=NONE
hi! link TabLine StatusLineNC
hi! link TabLineSel StatusLine

" Diff
hi DiffAdd        guibg=#738a90       guifg=#f2e5bc
hi DiffDelete     guibg=#8b3922       guifg=#f2e5bc
hi DiffChange     guibg=#689271       guifg=#f2e5bc

hi MySemicolon    guifg=#5d0000
hi MyColon        guifg=#5d0000
hi MyDot          guifg=#5d0000
hi MyComma        guifg=#5d0000
hi MyBrackets     guifg=#5d0000
hi MySqBrackets   guifg=#5d0000
hi MyCurlyBraces  guifg=#5d0000
hi MyOperators    guifg=#5d0000

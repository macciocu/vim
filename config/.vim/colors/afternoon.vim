" Vim color theme file
" author: Giovanni Macciocu
" date: Sun May 19 16:11:05 2019

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=dark
let g:colors_name="afternoon"

" Syntax
hi Normal         guibg=#191919       guifg=#b0b0b0       cterm=NONE
hi Comment        guibg=NONE          guifg=#776b53       cterm=NONE
hi Title          guibg=NONE          guifg=#e0c289       cterm=NONE " strings, markdown sections, html headers, strings
hi Special        guibg=NONE          guifg=#44a900       cterm=NONE " special characters in strings
hi Constant       guibg=NONE          guifg=#85cfec       cterm=NONE " numbers
hi Identifier     guibg=NONE          guifg=#E04B5A       cterm=NONE
hi Statement      guibg=NONE          guifg=#f55302       cterm=NONE
hi PreProc        guibg=NONE          guifg=#44a900       cterm=NONE
hi Type           guibg=NONE          guifg=#44a900       cterm=NONE
hi Underlined     guibg=NONE          guifg=#959595       cterm=NONE
hi Ignore         guibg=NONE          guifg=#d24300       cterm=NONE
hi Error          guibg=NONE          guifg=#d10e44       cterm=NONE
hi Wildmenu       guibg=#161616       guifg=#98971a       cterm=bold
hi SpellBad       guibg=#a83232       guifg=#000000       cterm=NONE " error highlight coloring
hi SpellCap       guibg=#a87d32       guifg=#000000       cterm=NONE " warning highlight coloring
hi! link String Title
hi! link Todo Error

" UI
hi LineNr         guibg=#191919       guifg=#4a3e27       cterm=NONE
hi NonText        guibg=#191919       guifg=#4a4544       cterm=NONE " e.g. '~' (end of buffer), terminal prompt
hi SpecialKey     guibg=NONE          guifg=#3f4151       cterm=NONE
hi Search         guibg=#738720       guifg=#d5d1ca       cterm=NONE
hi Visual         guibg=#505050       guifg=NONE          cterm=NONE
hi PMenu          guibg=#204e87       guifg=#ffffff       cterm=NONE
hi PMenuSel       guibg=#ffffff       guifg=#004c8e       cterm=NONE
hi Directory      guibg=NONE          guifg=#2070b2       cterm=bold
hi Folded         guibg=#101010       guifg=#7d7b77       cterm=NONE
hi FoldColumn     guibg=#101010       guifg=NONE          cterm=NONE
hi CursorLine     guibg=#212121       guifg=NONE          cterm=NONE
hi CursorLineNr   guibg=#212121       guifg=#978b73       cterm=NONE
hi StatusLine     guibg=#303030       guifg=#aaaaaa       cterm=bold
hi StatusLineNC   guibg=#222222       guifg=#aaaaaa       cterm=NONE
hi! link VertSplit LineNr
hi! link SignColumn LineNr
"hi! link StatusLineTerm StatusLine
"hi! link StatusLineTermNC StatusLineNC
hi! link MatchParen Search

" UI Tabs
hi TabLineFill    guibg=#252525       guifg=#ffffff       cterm=NONE
hi! link TabLine StatusLineNC
hi! link TabLineSel StatusLine

" Diff
hi DiffAdd        guibg=#207887       guifg=#ffffff
hi DiffDelete     guibg=#874520       guifg=#ffffff
hi DiffChange     guibg=#218639       guifg=#ffffff

hi MySemicolon    guifg=#E04B5A
hi MyColon        guifg=#E04B5A
hi MyDot          guifg=#E04B5A
hi MyComma        guifg=#E04B5A
hi MyBrackets     guifg=#E04B5A
hi MySqBrackets   guifg=#E04B5A
hi MyCurlyBraces  guifg=#E04B5A
hi MyOperators    guifg=#E04B5A

" Vim color theme file
" author: Giovanni Macciocu
" date: Sun May 19 16:11:05 2019

hi clear

if exists("syntax_on")
  syntax reset
endif

set background=dark
let g:colors_name="hc"

" Syntax
hi Normal         guibg=#101010       guifg=#659847
hi Comment        guibg=#101010       guifg=#7b7b5a       cterm=italic
hi String         guibg=NONE          guifg=#489fb5       cterm=NONE
hi Title          guibg=NONE          guifg=#ff5e55       cterm=NONE " markdown sections, html headers
hi Constant       guibg=NONE          guifg=#ebdbb2       cterm=NONE " numbers
hi Identifier     guibg=NONE          guifg=#82c0cc       cterm=NONE
hi Statement      guibg=NONE          guifg=#00cc80
hi PreProc        guibg=NONE          guifg=#c8790b       cterm=NONE
hi Type           guibg=NONE          guifg=#d15252       cterm=NONE
hi Special        guibg=NONE          guifg=#eec900       cterm=NONE
hi Underlined     guibg=NONE          guifg=#454545       cterm=NONE
hi Ignore         guibg=NONE          guifg=#dd5d0e       cterm=NONE
hi Error          guibg=NONE          guifg=#ff0000       cterm=NONE
hi SpellBad       guibg=#a87d32       guifg=#000000       cterm=NONE " error highlight coloring
hi SpellCap       guibg=#a83232       guifg=#000000       cterm=NONE " warning highlight coloring
hi! link Todo Error

" VimDiff
hi DiffAdd        guifg=NONE          guibg=#004000       cterm=NONE
hi DiffChange     guifg=NONE          guibg=#000040       cterm=NONE
hi DiffDelete     guifg=#ff8080       guibg=#400000       cterm=NONE
hi DiffText       guifg=NONE          guibg=#303030       cterm=NONE

" UI
hi LineNr         guibg=#101010       guifg=#32533d       cterm=NONE
hi NonText        guibg=NONE          guifg=NONE          cterm=NONE
hi SpecialKey     guibg=NONE          guifg=#3f4151       cterm=NONE
hi Search         guibg=#007050       guifg=#dddddd       cterm=NONE
hi Visual         guibg=#404040       guifg=NONE          cterm=NONE
hi PMenu          guibg=#004c8e       guifg=#ffffff       cterm=NONE
hi PMenuSel       guibg=#ffffff       guifg=#004c8e       cterm=NONE
hi Directory      guibg=NONE          guifg=#687bb4       cterm=NONE
hi Folded         guibg=#101010       guifg=#7d7b77       cterm=NONE
hi FoldColumn     guibg=#101010       guifg=NONE          cterm=NONE
hi CursorLine     guibg=#171717       guifg=NONE          cterm=NONE
hi CursorLineNr   guibg=#171717       guifg=#bf9900       cterm=bold
hi VertSplit      guibg=#101010       guifg=#454545       cterm=NONE
hi StatusLine     guibg=#255807       guifg=#000000       cterm=NONE
hi StatusLineNC   guibg=#353535       guifg=#000000       cterm=NONE
hi Wildmenu       guibg=#101010       guifg=#98971a       cterm=bold
hi! link NonText LineNr
hi! link SignColumn LineNr
hi! link StatusLineTerm StatusLine
hi! link StatusLineTermNC StatusLineNC
hi! link MatchParen Search
hi! link DiffAdd StatusLine
hi! link DiffDelete StatusLineNC


" UI Tabs
hi TabLineFill    guibg=#101010       guifg=#ffffff       cterm=NONE
hi! link TabLine StatusLineNC
hi! link TabLineSel StatusLine

" Diff
hi DiffAdd        guibg=#c0c0c0       guifg=#000000
hi DiffDelete     guibg=#730f4b       guifg=#000000
hi DiffChange     guibg=#32a852       guifg=#000000

hi MySemicolon    guifg=#f0f0f0
hi MyColon        guifg=#e3655b
hi MyDot          guifg=#f0f0f0
hi MyComma        guifg=#f0f0f0
hi MyBrackets     guifg=#f0f0f0
hi MySqBrackets   guifg=#f0f0f0
hi MyCurlyBraces  guifg=#f0f0f0
hi MyOperators    guifg=#f0f0f0
